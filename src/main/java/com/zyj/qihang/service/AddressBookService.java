package com.zyj.qihang.service;

import com.zyj.qihang.dto.AddressBook;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface AddressBookService extends IService<AddressBook> {

}
