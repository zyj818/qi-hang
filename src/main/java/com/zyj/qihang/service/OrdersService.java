package com.zyj.qihang.service;

import com.zyj.qihang.dto.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface OrdersService extends IService<Orders> {

    public void submit(Orders orders);
}
