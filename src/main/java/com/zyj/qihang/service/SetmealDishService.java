package com.zyj.qihang.service;

import com.zyj.qihang.dto.SetmealDish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface SetmealDishService extends IService<SetmealDish> {

}
