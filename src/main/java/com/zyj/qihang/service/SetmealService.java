package com.zyj.qihang.service;

import com.zyj.qihang.dto.Setmeal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyj.qihang.dto.SetmealDto;

import java.util.List;

/**
 *
 */
public interface SetmealService extends IService<Setmeal> {

    public void saveWithDish(SetmealDto setmealDto);

    public void deleteWithDish(List<Long> ids);

    public void updateStatusById(int status, List<String> ids);

    public SetmealDto getByIdWithDish(long id);

    public void updateWithDish(SetmealDto setmealDto);
}
