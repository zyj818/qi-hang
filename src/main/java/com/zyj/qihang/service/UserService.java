package com.zyj.qihang.service;

import com.zyj.qihang.dto.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface UserService extends IService<User> {

}
