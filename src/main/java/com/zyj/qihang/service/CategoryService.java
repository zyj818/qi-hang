package com.zyj.qihang.service;

import com.zyj.qihang.dto.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface CategoryService extends IService<Category> {
    public void remove(Long id);
}
