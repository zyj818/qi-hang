package com.zyj.qihang.service;

import com.zyj.qihang.dto.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface OrderDetailService extends IService<OrderDetail> {

}
