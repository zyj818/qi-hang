package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.dto.OrderDetail;
import com.zyj.qihang.service.OrderDetailService;
import com.zyj.qihang.mapper.OrderDetailMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail>
    implements OrderDetailService{

}




