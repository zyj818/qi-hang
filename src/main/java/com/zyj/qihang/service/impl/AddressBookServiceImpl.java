package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.dto.AddressBook;
import com.zyj.qihang.service.AddressBookService;
import com.zyj.qihang.mapper.AddressBookMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook>
    implements AddressBookService{

}




