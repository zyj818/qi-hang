package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.dto.DishFlavor;
import com.zyj.qihang.service.DishFlavorService;
import com.zyj.qihang.mapper.DishFlavorMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor>
    implements DishFlavorService{

}




