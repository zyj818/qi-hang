package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.dto.Employee;
import com.zyj.qihang.service.EmployeeService;
import com.zyj.qihang.mapper.EmployeeMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee>
    implements EmployeeService{

}




