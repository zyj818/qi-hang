package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.dto.SetmealDish;
import com.zyj.qihang.service.SetmealDishService;
import com.zyj.qihang.mapper.SetmealDishMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish>
    implements SetmealDishService{

}




