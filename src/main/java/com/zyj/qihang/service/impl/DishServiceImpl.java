package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.common.CustomException;
import com.zyj.qihang.dto.Dish;
import com.zyj.qihang.dto.DishDto;
import com.zyj.qihang.dto.DishFlavor;
import com.zyj.qihang.mapper.DishFlavorMapper;
import com.zyj.qihang.service.DishFlavorService;
import com.zyj.qihang.service.DishService;
import com.zyj.qihang.mapper.DishMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish>
    implements DishService{
    @Autowired
    private DishFlavorService dishFlavorService;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;


    @Override
    public void saveWithFlavor(DishDto dishDto) {
        this.save(dishDto);

        Long id = dishDto.getId();
        List<DishFlavor> list = dishDto.getFlavors();
        list.stream().map((item) ->{
            item.setDishId(id);
            return item;
        }).collect(Collectors.toList());

        dishFlavorService.saveBatch(list);
    }

    @Override
    public DishDto getByIdWithFlavor(Long id) {
        Dish dish = this.getById(id);

        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);


        LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(DishFlavor::getDishId,id);
        List<DishFlavor> list = dishFlavorService.list(lambdaQueryWrapper);

        dishDto.setFlavors(list);

        return dishDto;
    }

    @Override
    public void updateWithFlavor(DishDto dishDto) {
        this.updateById(dishDto);

        Long id = dishDto.getId();
        LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(DishFlavor::getDishId,id);
        dishFlavorService.remove(lambdaQueryWrapper);

        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors.stream().map((item) -> {
            item.setDishId(id);
            return item;
        }).collect(Collectors.toList());

        dishFlavorService.saveBatch(flavors);

    }

    @Override
    @Transactional
    public void deleteDishAndFlavor(List<String> ids) {
        //删除的同时还要删除照片   待百度
        LambdaQueryWrapper<Dish> QueryWrapper = new LambdaQueryWrapper<>();
        QueryWrapper.eq(Dish::getStatus,1);
        QueryWrapper.in(Dish::getId,ids);
        int count = count(QueryWrapper);
        if (count > 0){
            throw new CustomException("菜品正在使用,请先停售");
        }

        this.removeByIds(ids);
        LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(DishFlavor::getDishId,ids);
        dishFlavorService.remove(lambdaQueryWrapper);
//        for (String id : ids) {
//            LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//            lambdaQueryWrapper.eq(DishFlavor::getDishId,id);
//            dishFlavorService.remove(lambdaQueryWrapper);
//        }
    }

    @Override
    @Transactional
    public void updateStatusById(int status, List<String> ids) {
//        for (String id : ids) {
//            LambdaUpdateWrapper<Dish> dishLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
//            dishLambdaUpdateWrapper.set(Dish::getStatus,status);
//            dishLambdaUpdateWrapper.eq(Dish::getId,id);
//            this.update(dishLambdaUpdateWrapper);
//        }
                LambdaUpdateWrapper<Dish> dishLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
                dishLambdaUpdateWrapper.set(Dish::getStatus,status);
                dishLambdaUpdateWrapper.in(Dish::getId,ids);
                this.update(dishLambdaUpdateWrapper);
    }
}




