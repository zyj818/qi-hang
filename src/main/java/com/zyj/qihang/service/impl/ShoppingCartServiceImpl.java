package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.dto.ShoppingCart;
import com.zyj.qihang.service.ShoppingCartService;
import com.zyj.qihang.mapper.ShoppingCartMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart>
    implements ShoppingCartService{

}




