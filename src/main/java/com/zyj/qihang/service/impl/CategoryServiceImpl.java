package com.zyj.qihang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyj.qihang.common.CustomException;
import com.zyj.qihang.common.R;
import com.zyj.qihang.dto.Category;
import com.zyj.qihang.dto.Dish;
import com.zyj.qihang.dto.Setmeal;
import com.zyj.qihang.dto.SetmealDish;
import com.zyj.qihang.service.CategoryService;
import com.zyj.qihang.mapper.CategoryMapper;
import com.zyj.qihang.service.DishService;
import com.zyj.qihang.service.SetmealDishService;
import com.zyj.qihang.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService{
    @Autowired
    private DishService dishService;
    @Autowired
    private SetmealService setmealService;

    @Transactional
    public void remove(Long id){
        //1、判断有无菜品分类在使用
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);
        int count = dishService.count(dishLambdaQueryWrapper);
        if (count > 0){
            throw new CustomException("该菜品分类正在使用");
        }
        //2、判断有无套餐分类在使用
        LambdaQueryWrapper<Setmeal> dishLambdaQueryWrapper1 = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper1.eq(Setmeal::getCategoryId,id);
        int count1 = setmealService.count(dishLambdaQueryWrapper1);
        if (count1 > 0){
            throw new CustomException("该套餐正在使用");
        }
        //3、删除
        super.removeById(id);
    }
}




