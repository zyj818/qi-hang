package com.zyj.qihang.service;

import com.zyj.qihang.dto.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface EmployeeService extends IService<Employee> {

}
