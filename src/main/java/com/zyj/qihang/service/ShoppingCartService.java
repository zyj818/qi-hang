package com.zyj.qihang.service;

import com.zyj.qihang.dto.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ShoppingCartService extends IService<ShoppingCart> {

}
