package com.zyj.qihang.service;

import com.zyj.qihang.dto.Dish;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyj.qihang.dto.DishDto;

import java.util.List;

/**
 *
 */
public interface DishService extends IService<Dish> {

    public void saveWithFlavor(DishDto dishDto);

    public DishDto getByIdWithFlavor(Long id) ;

    public void updateWithFlavor(DishDto dishDto);

    public void deleteDishAndFlavor(List<String> ids);

    public void updateStatusById(int status, List<String> ids);
}
