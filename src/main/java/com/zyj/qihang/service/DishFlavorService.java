package com.zyj.qihang.service;

import com.zyj.qihang.dto.DishFlavor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface DishFlavorService extends IService<DishFlavor> {

}
