package com.zyj.qihang.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zyj.qihang.common.R;
import com.zyj.qihang.dto.User;
import com.zyj.qihang.service.UserService;
import com.zyj.qihang.util.SMSUtil;
import com.zyj.qihang.util.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    //持久层实现层使用ioc自动注入 使用@componet或者持久层注解便可  不用理会该错误
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private UserService userService;

    @PostMapping("/sendMsg")
    public R<String> getMsgCode(@RequestBody User user){
        //先进行查询是否存在redis中
        String code = stringRedisTemplate.opsForValue().get(user.getPhone());
        if (code != null){
            log.info("验证码已存在:{}",code);
//            Boolean flag = SMSUtil.SendMsg(user.getPhone(), "your_singName", "your_templateCode", code);
            return R.success("验证码已发送");
        }

        code = ValidateCodeUtils.generateValidateCode4String(4);
        stringRedisTemplate.opsForValue().set(user.getPhone(),code,60, TimeUnit.SECONDS);
        log.info("验证码为:{}",code);
//            Boolean flag = SMSUtil.SendMsg(user.getPhone(), "your_singName", "your_templateCode", code);
        return R.success("短信发送成功");
    }

    @PostMapping("/login")
    public R<User> login(@RequestBody Map<String,String> map, HttpSession session){
        String phone = map.get("phone");
        String code = map.get("code");

        String redisCode = stringRedisTemplate.opsForValue().get(phone);

        if (redisCode.equals(code)){
            //如果能够比对成功，说明登录成功

            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone,phone);

            User user = userService.getOne(queryWrapper);
            if(user == null){
                //判断当前手机号对应的用户是否为新用户，如果是新用户就自动完成注册
                user = new User();
                user.setPhone(phone);
                user.setStatus(1);
                userService.save(user);
            }
            session.setAttribute("user",user.getId());
            return R.success(user);
        }



        return R.error("验证失败");
    }

}
