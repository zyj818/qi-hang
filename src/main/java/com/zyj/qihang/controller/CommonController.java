package com.zyj.qihang.controller;

import com.sun.deploy.net.HttpResponse;
import com.zyj.qihang.common.R;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

@RestController
@RequestMapping("/common")
public class CommonController {
    @Value("${qihang.path}")
    private String basePath;

    @PostMapping("/upload")
    public R<String> upload(MultipartFile file){
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String newName = UUID.randomUUID().toString() + suffix;

        File file1 = new File(basePath);
        if (!file1.exists()){
            file1.mkdirs();
        }

        try {
            file.transferTo(new File(basePath + newName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.success(newName);
    }

    @GetMapping("/download")
    public void download(String name, HttpServletResponse response, String fileType){

        try {
            //创建输入流读取文件
            FileInputStream fileInputStream = new FileInputStream(new File(basePath + name));

            //1、创建输出流写入文件
            response.setContentType(fileType);
            ServletOutputStream outputStream = response.getOutputStream();

            byte[] bytes = new byte[1024];
            int len = 0;
            while ((len = fileInputStream.read(bytes)) != -1){
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }
            //关闭资源
            outputStream.close();
            fileInputStream.close();

            //2、使用io包的IoUtils.copy(inputstream,outputstream)
            //          <dependency>
            //            <groupId>org.apache.commons</groupId>
            //            <artifactId>commons-io</artifactId>
            //            <version>1.3.2</version>
            //          </dependency>
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
