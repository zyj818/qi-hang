package com.zyj.qihang.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyj.qihang.common.R;
import com.zyj.qihang.common.ThreadUtil;
import com.zyj.qihang.dto.Orders;
import com.zyj.qihang.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrdersService ordersService;

    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){
        ordersService.submit(orders);
        return R.success("下单成功");
    }

    @GetMapping("/userPage")
    public R<Page<Orders>> page(int page, int pageSize){
        Page<Orders> Page = new Page<>(page, pageSize);
        Long currentId = ThreadUtil.getCurrentId();
        LambdaQueryWrapper<Orders> ordersLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ordersLambdaQueryWrapper.eq(Orders::getUserId,currentId);
        ordersService.page(Page,ordersLambdaQueryWrapper);
        return R.success(Page);
    }

    @GetMapping("/page")
    public R<Page<Orders>> adminPage(int page, int pageSize,String number,String beginTime, String endTime){
        Page<Orders> Page = new Page<>(page, pageSize);
        LambdaQueryWrapper<Orders> ordersLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ordersLambdaQueryWrapper.eq(number != null,Orders::getNumber,number);
        ordersLambdaQueryWrapper.between(!StringUtils.isBlank(beginTime) && !StringUtils.isBlank(endTime),Orders::getOrderTime,beginTime,endTime);
        ordersService.page(Page,ordersLambdaQueryWrapper);
        return R.success(Page);
    }
}
