package com.zyj.qihang;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.zyj.qihang.mapper")
@EnableTransactionManagement
//@ServletComponentScan
public class QihangTakeOutApplication {

    public static void main(String[] args) {
        SpringApplication.run(QihangTakeOutApplication.class, args);
    }

}
