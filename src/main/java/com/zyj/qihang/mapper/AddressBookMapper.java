package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.AddressBook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.AddressBook
 */
public interface AddressBookMapper extends BaseMapper<AddressBook> {

}




