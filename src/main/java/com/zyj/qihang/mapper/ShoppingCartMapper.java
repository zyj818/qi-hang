package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.ShoppingCart
 */
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

}




