package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.Category
 */
public interface CategoryMapper extends BaseMapper<Category> {

}




