package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.DishFlavor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @Entity com.zyj.qihang.dto.DishFlavor
 */
@Repository
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {

}




