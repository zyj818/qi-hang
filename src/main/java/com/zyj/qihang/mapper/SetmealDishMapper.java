package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.SetmealDish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.SetmealDish
 */
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {

}




