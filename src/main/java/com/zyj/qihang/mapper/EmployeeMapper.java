package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.Employee
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}




