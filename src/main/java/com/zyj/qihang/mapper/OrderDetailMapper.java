package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.OrderDetail
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}




