package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.Setmeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.Setmeal
 */
public interface SetmealMapper extends BaseMapper<Setmeal> {

}




