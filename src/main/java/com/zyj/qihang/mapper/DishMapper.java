package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.Dish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @Entity com.zyj.qihang.dto.Dish
 */

public interface DishMapper extends BaseMapper<Dish> {

}




