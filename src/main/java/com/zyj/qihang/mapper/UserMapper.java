package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.User
 */
public interface UserMapper extends BaseMapper<User> {

}




