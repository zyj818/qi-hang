package com.zyj.qihang.mapper;

import com.zyj.qihang.dto.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.zyj.qihang.dto.Orders
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}




