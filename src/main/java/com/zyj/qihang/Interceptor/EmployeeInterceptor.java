package com.zyj.qihang.Interceptor;

import com.alibaba.fastjson.JSON;
import com.zyj.qihang.common.R;
import com.zyj.qihang.common.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Slf4j
public class EmployeeInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String path = request.getRequestURI();


        HttpSession session = request.getSession();
        Object employee = session.getAttribute("employee");
        if (employee != null){
            ThreadUtil.setCurrentId((Long) employee);
            return true;
        }

        Object user = session.getAttribute("user");
        if (user != null){
            ThreadUtil.setCurrentId((Long) user);
            return true;
        }

        log.info("拦截的路径是:{}",path);



        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return false;
    }
}